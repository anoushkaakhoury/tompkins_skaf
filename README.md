# Overview
- This repository contains the following folders for resource creation:
 
    - 01-state-storage-backend : for storing remote state in S3 bucket and DynamoDB. 
    - 02-network : To create a VPC with relevant resources and Bastion host with its security group.
    - 03-eks: To create Elastic Kubernetes Service cluster and node groups.
    - 04-cis-rules-alarms: To deploy CIS benchmark alarms and triggers.

```NOTE: REFER TO INTERNAL MODULE README FOR MORE DETAILS ON EVERY MODULE ```

## Pre-requisites:
### Install Below utilities
1. Terraform 1.0.0 :- https://www.terraform.io/docs/cli/install/apt.html
2. Kubectl 1.21.0 :- https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
3. Helm 3.0.0 :- https://helm.sh/docs/intro/install/
4. Aws cli 2:- https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html
5. JQ :- https://stedolan.github.io/jq/download/

## Deployment
**1. To store remote state in an S3 bucket and DynamoDB :**

    cd aws/01-state-storage-backend
    
  - To pass custom values for S3 bucket and region change ***backend.tfvars.reference*** in vars folder to ***backend.tfvars*** and pass your values there.
  - To apply

        terraform init
        terraform apply -var-file=vars/backend.tfvars

  - Save the output values
  - To destroy the resources:

        terraform destroy -var-file=vars/backend.tfvars

**2. To deploy vpc with private subnets and public subnets :**

- *This module creates a vpc along with Bastion-host and its security group.*
      cd aws/02-network
-   If a state-storage-backend was created then enter state-storage-backend output values in backend.tf
-  If a state-storage-backend was not created: remove backend.tf file 
 - To pass custom values for vpc edit ***vpc_secure_level1.tfvars.reference*** in vars folder and change it to ***vpc_secure_level1.tfvars*** and pass your values there
- To apply  :

        cd aws/02-network
        terraform init
        terraform plan -var-file=vars/vpc_secure_level1.tfvars -out plan.out   -->  for saving the plan consisting of terraform created resources in plan.out file
        terraform apply plan.out 

- Save the output values
- To destroy the resources:

      terraform destroy -var-file=vars/vpc_secure_level1.tfvars

- Bastion-host security group rules :

        i. ingress rule for port 22 from 0.0.0.0/0
        ii. egress rule for all ports from 0.0.0.0/0


**3. To deploy RDS**

    cd aws/03-rds
 - If a state-storage-backend was created then enter state-storage-backend output values in backend.tf
 - If a state-storage-backend was not created: remove backend.tf file 
 - To pass custom values for rds edit ***rds.tfvars.reference*** in vars folder to ***rds.tfvars*** and pass your values there.     
 - To apply  :

        cd aws/03-rds
        terraform init
        terraform plan -var-file=vars/rds.tfvars -out plan.out   -->  for saving the plan consisting of terraform created resources in plan.out file
        terraform apply plan.out 

  - Save the output values
  - To destroy the resources:

        terraform destroy -var-file=vars/rds.tfvars

  - For supported RDS instance classses and engine versions [Ref: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Concepts.DBInstanceClass.html]
  - For parameter group of RDS [Ref: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_WorkingWithParamGroups.html]

**4. To deploy DocumentDB**

    cd aws/04-documentdb

- If a state-storage-backend was created then enter state-storage-backend output values in backend.tf
- If a state-storage-backend was not created: remove backend.tf file 
 - To pass custom values for documentdb edit ***documentdb.tfvars.reference*** in vars folder to ***documentdb.tfvars*** and pass your values there.  
 - For supported engine versions [Ref: https://docs.aws.amazon.com/documentdb/latest/developerguide/db-cluster-create.html]   
 - For parameter group of DocumentDB cluster [Ref: https://docs.aws.amazon.com/documentdb/latest/developerguide/cluster_parameter_groups-create.html]
  - To apply  :

        cd aws/04-documentdb
        terraform init
        terraform plan -var-file=vars/documentdb.tfvars -out plan.out   -->  for saving the plan consisting of terraform created resources in plan.out file
        terraform apply plan.out 

- Save the output values
- To destroy the resources:

      terraform destroy -var-file=vars/documentdb.tfvars
      
**5. To deploy Elastic Kubernetes Service:**

    cd aws/05-eks

- If a state-storage-backend was created then enter state-storage-backend output values in backend.tf
- If a state-storage-backend was not created: remove backend.tf file
- To pass custom values for EKS edit ***eks_cluster.tfvars.reference*** in vars folder to ***eks_cluster.tfvars*** and pass your values there.
- To apply:-

    ```
    terraform init
    terraform validate
    terraform plan -var-file=vars/eks_cluster.tfvars -out plan.out  -->  for saving the plan consisting of terraform created resources in plan.out file
    terraform apply "plan.out"

    ```
- After Successful Deployment of source part, you will get the required outputs.

- To clean up the stack, run the following command -

    ```
    terraform destroy -var-file=vars/eks_cluster.tfvars

    ```

**6. To deploy RDS Aurora**

    cd aws/06-rds-aurora
 - If a state-storage-backend was created then enter state-storage-backend output values in backend.tf
 - If a state-storage-backend was not created: remove backend.tf file 
 - To pass custom values for rds edit ***rds_aurora.tfvars.reference*** in vars folder to ***rds_aurora.tfvars*** and pass your values there.     
  - To apply  :

        cd aws/06-rds-aurora
        terraform init
        terraform plan -var-file=vars/rds_aurora.tfvars -out plan.out   -->  for saving the plan consisting of terraform created resources in plan.out file
        terraform apply plan.out 

  - Save the output values
  - To destroy the resources:

        terraform destroy -var-file=vars/rds_aurora.tfvars

  - For supported RDS instance classses and engine versions [Ref: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Concepts.DBInstanceClass.html]
  - For parameter group of RDS [Ref: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_WorkingWithParamGroups.html]

**7. To deploy CIS Config Rules and CLoudwatch Alarms**

    cd aws/06-cis-rules-aalrms
 - If a state-storage-backend was created then enter state-storage-backend output values in backend.tf
 - If a state-storage-backend was not created: remove backend.tf file 
 - To pass custom values for rds edit ***terraform.tfvars.reference*** in vars folder to ***terraform.tfvars*** and pass your values there.     
  - To apply  :

        cd aws/06-cis-rules-alarms
        terraform init
        terraform plan -var-file=vars/terraform.tfvars -out plan.out   -->  for saving the plan consisting of terraform created resources in plan.out file
        terraform apply plan.out 

  - Save the output values
  - To destroy the resources:

        terraform destroy -var-file=vars/terraform.tfvars


## CIS COMPLIANCE 

```
Check individual module's README.mdfor CIS compliance compatibility.
```