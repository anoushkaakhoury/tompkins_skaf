variable "nfs_storage_size" {
  default = "20Gi"
  type    = string
}