data "aws_caller_identity" "current" {}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

resource "tls_private_key" "eks_keypair" {
  algorithm = "RSA"
}

resource "local_file" "eks_keypair_private_key" {
  content         = tls_private_key.eks_keypair.private_key_pem
  filename        = format("%s-%s", module.eks.cluster_id, "ssh-key.pem")
  file_permission = "0600"
}

module "key_pair" {
  source     = "terraform-aws-modules/key-pair/aws"
  version    = "0.6.0"
  key_name   = format("%s-%s-%s", module.eks.cluster_id, "key-pair", var.environment)
  public_key = tls_private_key.eks_keypair.public_key_openssh
}

resource "aws_kms_key" "eks" {
  description             = "EKS Secret Encryption Key"
  enable_key_rotation     = true
  deletion_window_in_days = 7

  policy = <<EOT
{
    "Version": "2012-10-17",
    "Id": "terraform-key",
    "Statement": [
        {
            "Sid": "Enable IAM User Permissions",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
            },
            "Action": "kms:*",
            "Resource": "*"
        },
        {
            "Sid": "Allow access for Key Administrators",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/autoscaling.amazonaws.com/AWSServiceRoleForAutoScaling",
                    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/eks.amazonaws.com/AWSServiceRoleForAmazonEKS"
                ]
            },
            "Action": [
                "kms:Create*",
                "kms:Describe*",
                "kms:Enable*",
                "kms:List*",
                "kms:Put*",
                "kms:Update*",
                "kms:Revoke*",
                "kms:Disable*",
                "kms:Get*",
                "kms:Delete*",
                "kms:TagResource",
                "kms:UntagResource",
                "kms:ScheduleKeyDeletion",
                "kms:CancelKeyDeletion"
            ],
            "Resource": "*"
        },
        {
            "Sid": "Allow use of the key",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/autoscaling.amazonaws.com/AWSServiceRoleForAutoScaling",
                    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/eks.amazonaws.com/AWSServiceRoleForAmazonEKS"
                ]
            },
            "Action": [
                "kms:Encrypt",
                "kms:Decrypt",
                "kms:ReEncrypt*",
                "kms:GenerateDataKey*",
                "kms:DescribeKey"
            ],
            "Resource": "*"
        },
        {
            "Sid": "Allow attachment of persistent resources",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/autoscaling.amazonaws.com/AWSServiceRoleForAutoScaling",
                    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/aws-service-role/eks.amazonaws.com/AWSServiceRoleForAmazonEKS"
                ]
            },
            "Action": [
                "kms:CreateGrant",
                "kms:ListGrants",
                "kms:RevokeGrant"
            ],
            "Resource": "*",
            "Condition": {
                "Bool": {
                    "kms:GrantIsForAWSResource": "true"
                }
            }
        }
    ]
}
EOT
}

resource "aws_iam_policy" "kubernetes_pvc_kms_policy" {
  name        = format("%s-%s", module.eks.cluster_id, "kubernetes-pvc-kms-policy")
  description = "Allow kubernetes pvc to get access of KMS."

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
      {
        "Effect": "Allow",
        "Action": [
            "kms:CreateGrant",
            "kms:Decrypt",
            "kms:GenerateDataKeyWithoutPlaintext"
        ],
        "Resource": "${aws_kms_key.eks.arn}"
      }
  ]
}
EOF  
}

data "aws_iam_policy" "SSMManagedInstanceCore" {
  arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_role_policy_attachment" "SSMManagedInstanceCore_attachment" {
  role       = module.eks.worker_iam_role_name
  policy_arn = data.aws_iam_policy.SSMManagedInstanceCore.arn
}


resource "aws_iam_role_policy_attachment" "eks_kms_key_policy_attachment" {
  role       = module.eks.cluster_iam_role_name
  policy_arn = aws_iam_policy.kubernetes_pvc_kms_policy.arn
}


data "aws_ami" "launch_template_ami" {
  owners      = ["602401143452"] ## Amazon Account Id
  most_recent = true

  filter {
    name   = "name"
    values = ["amazon-eks-node-${var.cluster_version}-v*"]
  }
}



data "template_file" "launch_template_userdata" {
  template = file("./templates/custom-bootstrap-script.sh.tpl")

  vars = {
    cluster_name                 = module.eks.cluster_id
    endpoint                     = module.eks.cluster_endpoint
    cluster_auth_base64          = module.eks.cluster_certificate_authority_data
    image_high_threshold_percent = var.image_high_threshold_percent
    image_low_threshold_percent  = var.image_low_threshold_percent
    eventRecordQPS               = var.eventRecordQPS
  }
}

resource "aws_launch_template" "eks_template_infra" {
  name            = format("%s-%s", module.eks.cluster_id, "launch-template-infra")
  default_version = 1
  key_name        = module.key_pair.this_key_pair_key_name
  user_data       = base64encode(data.template_file.launch_template_userdata.rendered)
  image_id        = data.aws_ami.launch_template_ami.image_id

  block_device_mappings {
    device_name  = "/dev/xvda"

    ebs {
      volume_size           = var.root_volume_infra_gb
      volume_type           = "gp2"
      delete_on_termination = true
      encrypted             = true
      kms_key_id            = aws_kms_key.eks.arn
    }
  }

  network_interfaces {
    associate_public_ip_address = false
    delete_on_termination       = true
    security_groups             = [module.eks.cluster_primary_security_group_id]
  }

  monitoring {
    enabled = true
  }

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = format("%s-%s-%s", var.environment, var.name, "worker-node-infra")
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_launch_template" "eks_template_app_demand" {
  name            = format("%s-%s", module.eks.cluster_id, "launch-template-app-on-demand")
  default_version = 1
  key_name        = module.key_pair.this_key_pair_key_name
  user_data       = base64encode(data.template_file.launch_template_userdata.rendered)
  image_id        = data.aws_ami.launch_template_ami.image_id

  block_device_mappings {
    device_name  = "/dev/xvda"

    ebs {
      volume_size           = var.root_volume_app_gb_demand
      volume_type           = "gp2"
      delete_on_termination = true
      encrypted             = true
      kms_key_id            = aws_kms_key.eks.arn
    }
  }

  network_interfaces {
    associate_public_ip_address = false
    delete_on_termination       = true
    security_groups             = [module.eks.cluster_primary_security_group_id]
  }

  monitoring {
    enabled = true
  }

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = format("%s-%s-%s", var.environment, var.name, "worker-node-app-on-demand")
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_launch_template" "eks_template_artemis" {
  name            = format("%s-%s", module.eks.cluster_id, "launch-template-artemis")
  default_version = 1
  key_name        = module.key_pair.this_key_pair_key_name
  user_data       = base64encode(data.template_file.launch_template_userdata.rendered)
  image_id        = data.aws_ami.launch_template_ami.image_id

  block_device_mappings {
    device_name  = "/dev/xvda"

    ebs {
      volume_size           = var.root_volume_gb_artemis
      volume_type           = "gp2"
      delete_on_termination = true
      encrypted             = true
      kms_key_id            = aws_kms_key.eks.arn
    }
  }

  network_interfaces {
    associate_public_ip_address = false
    delete_on_termination       = true
    security_groups             = [module.eks.cluster_primary_security_group_id]
  }

  monitoring {
    enabled = true
  }

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = format("%s-%s-%s", var.environment, var.name, "worker-node-artemis")
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_launch_template" "eks_template_database_mongo" {
  name            = format("%s-%s", module.eks.cluster_id, "launch-template-database_mongo")
  default_version = 1
  key_name        = module.key_pair.this_key_pair_key_name
  user_data       = base64encode(data.template_file.launch_template_userdata.rendered)
  image_id        = data.aws_ami.launch_template_ami.image_id

  block_device_mappings {
    device_name  = "/dev/xvda"

    ebs {
      volume_size           = var.root_volume_database_mongo_gb
      volume_type           = "gp2"
      delete_on_termination = true
      encrypted             = true
      kms_key_id            = aws_kms_key.eks.arn
    }
  }

  network_interfaces {
    associate_public_ip_address = false
    delete_on_termination       = true
    security_groups             = [module.eks.cluster_primary_security_group_id]
  }

  monitoring {
    enabled = true
  }

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = format("%s-%s-%s", var.environment, var.name, "database_mongo")
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_launch_template" "eks_template_database_mysql" {
  name            = format("%s-%s", module.eks.cluster_id, "launch-template-database_mysql")
  default_version = 1
  key_name        = module.key_pair.this_key_pair_key_name
  user_data       = base64encode(data.template_file.launch_template_userdata.rendered)
  image_id        = data.aws_ami.launch_template_ami.image_id

  block_device_mappings {
    device_name  = "/dev/xvda"

    ebs {
      volume_size           = var.root_volume_database_mysql_gb
      volume_type           = "gp2"
      delete_on_termination = true
      encrypted             = true
      kms_key_id            = aws_kms_key.eks.arn
    }
  }

  network_interfaces {
    associate_public_ip_address = false
    delete_on_termination       = true
    security_groups             = [module.eks.cluster_primary_security_group_id]
  }

  monitoring {
    enabled = true
  }

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = format("%s-%s-%s", var.environment, var.name, "database_mysql")
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}


module "eks" {
  source                    = "terraform-aws-modules/eks/aws"
  version                   = "17.1.0"
  cluster_name              = format("%s-%s", var.environment, var.name)
  cluster_enabled_log_types = var.cluster_enabled_log_types
  subnets                   = var.private_subnet_ids
  cluster_version           = var.cluster_version
  enable_irsa               = true

  tags = {
    "Name"           = format("%s-%s", var.environment, var.name)
    "Environment"    = var.environment
    "Infra-Services" = "true"
  }

  vpc_id                               = var.vpc_id
  manage_aws_auth                      = false
  write_kubeconfig                     = false
  cluster_log_retention_in_days        = var.cluster_log_retention_in_days
  cluster_endpoint_private_access      = var.cluster_endpoint_private_access
  cluster_endpoint_public_access       = var.cluster_endpoint_public_access
  cluster_endpoint_public_access_cidrs = var.cluster_endpoint_public_access_cidrs

  cluster_encryption_config = [
    {
      provider_key_arn = aws_kms_key.eks.arn
      resources        = ["secrets"]
    }
  ]

  node_groups = {
    eks_nodes_1 = {
      name                    = var.infra_ng_name
      desired_capacity        = var.desired_capacity_infra
      min_capacity            = var.min_capacity_infra
      max_capacity            = var.max_capacity_infra
      subnets                 = [var.private_subnet_ids[0]]
      capacity_type           = var.capacity_type_infra
      instance_types          = var.instance_type_infra
      launch_template_id      = aws_launch_template.eks_template_infra.id
      launch_template_version = aws_launch_template.eks_template_infra.default_version

      additional_tags = merge(var.additional_tags,
        {

          "kubernetes.io/cluster/${var.environment}-${var.name}" = "owned"
        },
        {
          "k8s.io/cluster-autoscaler/${var.environment}-${var.name}" = "owned"
        },
        {
          "k8s.io/cluster-autoscaler/enabled" = "true"
        },
        {
          "Name" = format("%s-%s-%s", var.environment, var.name, var.infra_ng_name)
        }
      )

      k8s_labels = {
        "Infra-Services" = "true"
      }
    },
    eks_nodes_2 = {
      name                    = format("%s-ON-DEMAND", var.application_ng_name_demand)
      desired_capacity        = var.desired_capacity_app_demand
      min_capacity            = var.min_capacity_app_demand
      max_capacity            = var.max_capacity_app_demand
      subnets                 = var.private_subnet_ids
      capacity_type           = var.capacity_type_app_demand
      instance_types          = var.instance_type_app_demand
      launch_template_id      = aws_launch_template.eks_template_app_demand.id
      launch_template_version = aws_launch_template.eks_template_app_demand.default_version

      additional_tags = merge(var.additional_tags,
        {
          "kubernetes.io/cluster/${var.environment}-${var.name}" = "owned"
        },
        {
          "k8s.io/cluster-autoscaler/${var.environment}-${var.name}" = "owned"
        },
        {
          "k8s.io/cluster-autoscaler/enabled" = "true"
        },
        {
          "Name" = format("%s-%s-%s-on-demand", var.environment, var.name, var.application_ng_name_demand)
        }
      )
      k8s_labels = {
        "App-On-Demand" = "true"
      }
    },
    eks_nodes_3 = {
      name                    = format("%s-ARTEMIS", var.artemis_ng_name)
      desired_capacity        = var.desired_capacity_artemis
      min_capacity            = var.min_capacity_artemis
      max_capacity            = var.max_capacity_artemis
      subnets                 = var.private_subnet_ids
      capacity_type           = var.capacity_type_artemis
      instance_types          = var.instance_type_artemis
      launch_template_id      = aws_launch_template.eks_template_artemis.id
      launch_template_version = aws_launch_template.eks_template_artemis.default_version

      additional_tags = merge(var.additional_tags,
        {
          "kubernetes.io/cluster/${var.environment}-${var.name}" = "owned"
        },
        {
          "k8s.io/cluster-autoscaler/${var.environment}-${var.name}" = "owned"
        },
        {
          "k8s.io/cluster-autoscaler/enabled" = "true"
        },
        {
          "Name" = format("%s-%s-%s-artemis", var.environment, var.name, var.artemis_ng_name)
        }
      )
      k8s_labels = {
        "Artemis" = "true"
      }
    },
    eks_nodes_4 = {
      name                    = var.mongo_ng_name
      desired_capacity        = var.desired_capacity_database_mongo
      min_capacity            = var.min_capacity_database_mongo
      max_capacity            = var.max_capacity_database_mongo
      subnets                 = var.private_subnet_ids
      capacity_type           = var.capacity_type_database_mongo
      instance_types          = var.instance_type_database_mongo
      launch_template_id      = aws_launch_template.eks_template_database_mongo.id
      launch_template_version = aws_launch_template.eks_template_database_mongo.default_version

      additional_tags = merge(var.additional_tags,
        {
          "kubernetes.io/cluster/${var.environment}-${var.name}" = "owned"
        },
        {
          "k8s.io/cluster-autoscaler/${var.environment}-${var.name}" = "owned"
        },
        {
          "k8s.io/cluster-autoscaler/enabled" = "true"
        },
        {
          "Name" = format("%s-%s-%s", var.environment, var.name, var.mongo_ng_name)
        }
      )
      k8s_labels = {
        "Database-Mongo" = "true"
      }
    }
    eks_nodes_5 = {
      name                    = var.mysql_ng_name
      desired_capacity        = var.desired_capacity_database_mysql
      min_capacity            = var.min_capacity_database_mysql
      max_capacity            = var.max_capacity_database_mysql
      subnets                 = var.private_subnet_ids
      capacity_type           = var.capacity_type_database_mysql
      instance_types          = var.instance_type_database_mysql
      launch_template_id      = aws_launch_template.eks_template_database_mysql.id
      launch_template_version = aws_launch_template.eks_template_database_mysql.default_version

      additional_tags = merge(var.additional_tags,
        {
          "kubernetes.io/cluster/${var.environment}-${var.name}" = "owned"
        },
        {
          "k8s.io/cluster-autoscaler/${var.environment}-${var.name}" = "owned"
        },
        {
          "k8s.io/cluster-autoscaler/enabled" = "true"
        },
        {
          "Name" = format("%s-%s-%s", var.environment, var.name, var.mysql_ng_name)
        }
      )
      k8s_labels = {
        "Mysql" = "true"
      }
    }
  }

}

resource "aws_iam_role_policy_attachment" "node_autoscaler_policy" {
  policy_arn = aws_iam_policy.node_autoscaler_policy.arn
  role       = module.eks.worker_iam_role_name
}

resource "aws_iam_policy" "node_autoscaler_policy" {
  name        = format("%s-%s-%s-node-autoscaler-policy", var.environment, var.name, module.eks.cluster_id)
  path        = "/"
  description = "Node auto scaler policy for node groups."
  policy      = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeLaunchConfigurations",
                "autoscaling:DescribeTags",
                "autoscaling:SetDesiredCapacity",
                "autoscaling:TerminateInstanceInAutoScalingGroup",
                "ec2:DescribeLaunchTemplateVersions"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "null_resource" "get_kubeconfig" {
  depends_on = [module.eks]

  provisioner "local-exec" {
    command = "aws eks update-kubeconfig --name ${module.eks.cluster_id} --region ${var.region}"
  }
}