module "cluster_autoscaler" {
  source     = "./helm/cluster-autoscaler"
  depends_on = [null_resource.get_kubeconfig]

  region                     = var.region
  cluster_name               = module.eks.cluster_id
  cluster_autoscaler_version = var.cluster_autoscaler_version
}

module "metrics_server" {
  source     = "./helm/metrics-server"
  depends_on = [module.cluster_autoscaler]

  metrics_server_version = var.metrics_server_version
}

resource "null_resource" "keda_custom_metric" {   //KEDA tool for different custom metric for autoscaling
  depends_on = [module.metrics_server]

  provisioner "local-exec" {
    command = "kubectl apply -f ./files/keda.yaml"
  }
}

module "nginx_ingress_controller" {
  source     = "./helm/nginx-ingress-controller"
  depends_on = [module.metrics_server]

  ingress_nginx_enabled = var.ingress_nginx_enabled
  ingress_nginx_version = var.ingress_nginx_version
}

module "cert_manager" {
  source     = "./helm/cert-manager"
  depends_on = [module.metrics_server]

  cert_manager_enabled = var.cert_manager_enabled
  cert_manager_version = var.cert_manager_version
}

module "aws_load_balancer_controller" {
  source     = "./helm/aws-alb-ingress-controller"
  depends_on = [module.metrics_server]

  ingress_aws_alb_enabled   = var.ingress_aws_alb_enabled
  aws_load_balancer_version = var.aws_load_balancer_version
  environment               = var.environment
  provider_url              = module.eks.cluster_oidc_issuer_url
  clusterName               = module.eks.cluster_id
  region                    = var.region
}

module "external_secrets" {
  source     = "./helm/external_secrets"
  depends_on = [null_resource.get_kubeconfig]

  external_secret_enabled = var.external_secret_enabled
  provider_url            = module.eks.cluster_oidc_issuer_url
  cluster_id              = module.eks.cluster_id
  environment             = var.environment
  region                  = var.region
  name                    = var.name
}

module "nfs" {
  source     = "./helm/nfs"
  depends_on = [module.cert_manager]
  count      = var.create_nfs ? 1 : 0
  nfs_storage_size = var.nfs_storage_size
}