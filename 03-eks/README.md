# USAGE

- Used to create Amazon EKS(Amazon Elastic Kubernetes Service). 
- Contains the following features:
  1. Creates an EKS cluster with different node groups consisting of "SPOT" and "ON DEMAND" nodes.
  2. It contains cluster autoscaler.
  3. It contains metrics server.
  4. Creation of ingress nginx crontroller.
  5. Issues certificates with the help of Cluster Issuer.
  6. Cluster_log_types "api", "audit", "authenticator", "controllerManager", "scheduler" are sent to cloudwatch.
  7. Creation of custom EBS storage class with KMS Encryption.
  8. Created default storage class.
  9. Creates EFS and storage class.
  10. The cluster and all its components are encrypted by default.
  11. Add external secrets functionality to store secrets in any cloud or vault. 

- ***.pem*** file for Nodes is saved in the root of 05-eks module.

- To store the ***.tfstate*** file in provisioned cloud backend edit ***backend.tf***.

```
NOTE: The paremeter KEY in backend.tf is the path in the backend S3 bucket where the .tfstate for each module will be stored. Make sure that the path is not the same for different modules.
```

- To pass custom values for EKS edit ***eks_cluster.tfvars.reference*** in vars folder to ***eks_cluster.tfvars*** and pass your values there.

- To apply:-

    ```
    cd aws/05-eks
    terraform init
    terraform validate
    terraform plan -var-file=vars/eks_cluster.tfvars -out plan.out  -->  for saving the plan consisting of terraform created resources in plan.out file
    terraform apply "plan.out"

    ```
- After Successful Deployment of source part, you will get the required outputs.

- To clean up the stack, run the following command -

    ```
    terraform destroy -var-file=vars/eks_cluster.tfvars

    ```

# CIS COMPLIANCE 

- Follows the VPC recommendations of CIS Amazon Web Services Foundations Benchmark v1.4.0  

| 2.2.1 | Ensure EBS volume encryption is enabled                                                                            |   OK    |                   For EKS Cluster Worker Nodes created using this module