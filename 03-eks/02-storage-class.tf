data "template_file" "aws_custom_storage_class" {
  depends_on = [null_resource.get_kubeconfig]
  template   = file("./files/aws-custom-storage-class.yaml")

  vars = {
    encrypted = true
    kmskeyId  = join("", aws_kms_key.eks.*.key_id)
  }
}

resource "null_resource" "aws_custom_storage_class" {
  depends_on = [data.template_file.aws_custom_storage_class]

  provisioner "local-exec" {
    command = "kubectl apply -f -<<EOF\n${join("", data.template_file.aws_custom_storage_class.*.rendered)}\nEOF"
  }
}