## COMMON VARIABLES

variable "additional_tags" {
  description = "Tags for resources"
  type        = map(string)
  default = {
  }
}

variable "application_ng_name_demand" {
  default = ""
  type    = string
}

variable "artemis_ng_name" {
  default = ""
  type    = string
}

variable "aws_load_balancer_version" {
  default = ""
  type    = string
}

variable "block_duration_minutes" {
  default = 60
  type    = number
}

variable "capacity_type_app_demand" {
  default = ""
  type    = string
}

variable "capacity_type_artemis" {
  default = ""
  type    = string
}

variable "capacity_type_infra" {
  default = ""
  type    = string
}

variable "cert_manager_enabled" {
  default = false
  type    = bool
}

variable "cert_manager_version" {
  default = ""
  type    = string
}

variable "cluster_autoscaler_version" {
  default = ""
  type    = string
}


variable "cluster_enabled_log_types" {
  default = [""]
  type    = list(string)
}

variable "cluster_endpoint_private_access" {
  default = false
  type    = bool
}

variable "cluster_endpoint_public_access" {
  default = false
  type    = bool
}

variable "cluster_endpoint_public_access_cidrs" {
  default = [""]
  type    = list(string)
}

variable "cluster_log_retention_in_days" {
  default = 90
  type    = number
}

variable "cluster_version" {
  default = ""
  type    = string
}

variable "create_nfs" {
  default = false
  type    = bool
}

variable "nfs_storage_size" {
  default = "20Gi"
  type    = string
}

variable "desired_capacity_app_demand" {
  default = 3
  type    = number
}

variable "desired_capacity_artemis" {
  default = 3
  type    = number
}

variable "desired_capacity_infra" {
  default = 3
  type    = number
}

variable "environment" {
  default = ""
  type    = string
}

variable "eventRecordQPS" {
  default = 5
  type    = number
}

variable "external_secret_enabled" {
  default = true
  type    = bool
}

variable "image_high_threshold_percent" {
  default = 60
  type    = number
}

variable "image_low_threshold_percent" {
  default = 40
  type    = number
}

variable "infra_ng_name" {
  default = ""
  type    = string
}

variable "ingress_aws_alb_enabled" {
  default = false
  type    = bool
}

variable "ingress_nginx_enabled" {
  default = false
  type    = bool
}

variable "ingress_nginx_version" {
  default = ""
  type    = string
}

variable "instance_interruption_behavior" {
  default = ""
  type    = string
}

variable "instance_type_app_demand" {
  default = [""]
  type    = list(string)
}

variable "instance_type_artemis" {
  default = [""]
  type    = list(string)
}

variable "instance_type_infra" {
  default = [""]
  type    = list(string)
}

variable "max_capacity_app_demand" {
  default = 5
  type    = number
}

variable "max_capacity_artemis" {
  default = 5
  type    = number
}

variable "max_capacity_infra" {
  default = 5
  type    = number
}

variable "max_price" {
  default = null
  type    = string
}

variable "metrics_server_version" {
  default = ""
  type    = string
}

variable "min_capacity_app_demand" {
  default = 1
  type    = number
}

variable "min_capacity_artemis" {
  default = 1
  type    = number
}

variable "min_capacity_infra" {
  default = 1
  type    = number
}

variable "name" {
  default = ""
  type    = string
}

variable "region" {
  default = ""
  type    = string
}

variable "private_subnet_ids" {
  default = [""]
  type    = list(string)
}

variable "public_subnet_ids" {
  default = [""]
  type    = list(string)
}

variable "root_volume_app_gb_demand" {
  default = 20
  type    = number
}

variable "root_volume_gb_artemis" {
  default = 20
  type    = number
}

variable "root_volume_infra_gb" {
  default = 20
  type    = number
}

variable "vpc_id" {
  default = ""
  type    = string
}

variable "Warning" {
  default = "Warning!! !SAVE THIS PEM FILE FOR ACCESSING WORKER NODES !"
  type    = string
}

variable "mongo_ng_name" {
  default = null
  type    = string
}

variable "desired_capacity_database_mongo" {
  default = 3
  type    = number
}

variable "min_capacity_database_mongo" {
  default = 1
  type    = number
}

variable "max_capacity_database_mongo" {
  default = 5
  type    = number
}

variable "capacity_type_database_mongo" {
  default = ""
  type    = string
}

variable "instance_type_database_mongo" {
  default = [""]
  type    = list(string)
}

variable "root_volume_database_mongo_gb" {
  default = 20
  type    = number
}

variable "root_volume_database_mysql_gb" {
  default = 20
  type    = number
}

variable "mysql_ng_name" {
  default = null
  type    = string
}

variable "desired_capacity_database_mysql" {
  default = 3
  type    = number
}

variable "min_capacity_database_mysql" {
  default = 1
  type    = number
}

variable "max_capacity_database_mysql" {
  default = 5
  type    = number
}

variable "capacity_type_database_mysql" {
  default = ""
  type    = string
}

variable "instance_type_database_mysql" {
  default = [""]
  type    = list(string)
}