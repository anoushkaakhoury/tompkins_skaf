output "_1_region" {
  description = "AWS Region"
  value       = var.region
}

output "_2_cluster_name" {
  description = "Kubernetes Cluster Name"
  value       = module.eks.cluster_id
}

output "_3_environment" {
  description = "Environment Name"
  value       = var.environment
}

output "_4_cluster_endpoint" {
  description = "Endpoint for EKS control plane"
  value       = module.eks.cluster_endpoint
}

output "_5_cluster_security_group_id" {
  description = "Security group ids attached to the cluster control plane"
  value       = module.eks.cluster_primary_security_group_id
}

output "_6_kubeconfig_context_name" {
  description = "Name of the kubeconfig context"
  value       = module.eks.cluster_arn
}

output "_7_nginx_ingress_controller_dns_hostname" {
  description = "NGINX Ingress Controller DNS Hostname"
  value       = module.nginx_ingress_controller.nginx_ingress_controller_dns_hostname
}

output "_8_ingress_aws_alb_enabled" {
  description = "Is aws alb ingress is enabled or not?"
  value       = var.ingress_aws_alb_enabled
}

output "_9_ebs_encryption" {
  description = "Is AWS EBS encryption is enabled or not?"
  value       = "Encrypted by default"
}

output "_10_kms_key_id" {
  description = "KMS Key ID"
  value       = join("", aws_kms_key.eks.*.key_id)
}

output "_11_cluster_oidc_issuer_url" {
  description = "NGINX Ingress Controller DNS Hostname"
  value       = module.eks.cluster_oidc_issuer_url
}

output "_12_Warning" {
  value = var.Warning
}

output "_13_local_file" {
  description = "Path of pem file"
  value       = format("%s-%s-%s", path.module, module.eks.cluster_id, "ssh-key.pem")
}

output "_14_tls_private_key" {
  description = "Warning!! ! Please Save this for future use !"
  value       = nonsensitive(tls_private_key.eks_keypair.private_key_pem)
}

output "_15_nfs_storage_class_name" {
  value       = "nfs"
}