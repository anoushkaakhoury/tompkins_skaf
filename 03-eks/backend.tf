terraform {
  backend "s3" {
    region         = "ap-south-1"
    bucket         = "dev-tompkins-097585176334"
    key            = "dev-tompkins-skaf-stack/eks-cluster/terraform.tfstate"
    dynamodb_table = "dev-tompkins-lock-dynamodb-097585176334"
  }
}