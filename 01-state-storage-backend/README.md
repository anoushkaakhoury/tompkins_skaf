# USAGE 

- To store ***.tfstate*** file in AWS cloud backend.
- Provisions an S3 bucket and DynamoDB table for storing .tfstate file. 

- To apply change the name of ***backend.tfvars.reference*** to ***backend.tfvars*** and enter ***region*** and ***bucket_name***.

- To apply:

``` 
terraform init 
terraform plan -var-file=vars/backend.tfvars -out plan.out
terraform apply plan.out
```

- **SAVE THE OUTPUTS**

- To destroy:

```
terraform destroy -var-file=vars/backend.tfvars"
```
# CIS COMPLIANCE 
- Also provisions an S3 bucket for storing logs with cloudtrail and cloudwatch integration for CIS compliance for Remote state storage S3 bucket only. [ CIS Amazon Web Services Foundations Benchmark   v1.4.0 ]

- Refer to the file compliance.md