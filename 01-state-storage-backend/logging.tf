resource "aws_cloudtrail" "s3_cloudtrail" {
  depends_on                    = [aws_iam_role_policy_attachment.s3_cloudtrail_policy_attachment]
  name                          = format("%s-%s-S3", var.bucket_name, data.aws_caller_identity.current.account_id)
  s3_bucket_name                = module.log_bucket.s3_bucket_id
  s3_key_prefix                 = "log"
  include_global_service_events = true
  enable_logging                = true
  enable_log_file_validation    = true
  cloud_watch_logs_role_arn     = aws_iam_role.s3_cloudtrail_cloudwatch_role.arn
  is_multi_region_trail         = true
  cloud_watch_logs_group_arn    = "${aws_cloudwatch_log_group.s3_cloudwatch.arn}:*"
  kms_key_id                    = module.kms_key.key_arn
  event_selector {
  read_write_type           = "All"
  include_management_events = true
    data_resource {
      type   = "AWS::S3::Object"
      values = ["arn:aws:s3"]
    }
  }
}

resource "aws_cloudwatch_log_group" "s3_cloudwatch" {
  name = format("%s-%s-S3", var.bucket_name, data.aws_caller_identity.current.account_id)
  kms_key_id        = module.kms_key.key_arn
  provisioner "local-exec" {
    command = "sleep 10"
  }
}

resource "aws_iam_role" "s3_cloudtrail_cloudwatch_role" {
  name = format("%s-cloudtrail-cloudwatch-S3", var.bucket_name)
  assume_role_policy = data.aws_iam_policy_document.cloudtrail_assume_role.json
}

data "aws_iam_policy_document" "cloudtrail_assume_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "s3_cloudtrail_cloudwatch_policy" {
  name   = format("%s-cloudtrail-cloudwatch-S3", var.bucket_name)
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AWSCloudTrailCreateLogStream2014110",
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogStream"
      ],
      "Resource": [
        "arn:aws:logs:${var.region}:${data.aws_caller_identity.current.account_id}:log-group:${var.bucket_name}-${data.aws_caller_identity.current.account_id}-S3:log-stream:*"
      ]
    },
    {
      "Sid": "AWSCloudTrailPutLogEvents20141101",
      "Effect": "Allow",
      "Action": [
        "logs:PutLogEvents"
      ],
      "Resource": [
        "arn:aws:logs:${var.region}:${data.aws_caller_identity.current.account_id}:log-group:${var.bucket_name}-${data.aws_caller_identity.current.account_id}-S3:log-stream:*"
      ]
    }
  ]
}
EOF
}
 
resource "aws_iam_role_policy_attachment" "s3_cloudtrail_policy_attachment"{
  role       = aws_iam_role.s3_cloudtrail_cloudwatch_role.name
  policy_arn = aws_iam_policy.s3_cloudtrail_cloudwatch_policy.arn
}

module "log_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"
  version = "2.9.0"
  bucket                                = format("%s-%s-log-bucket", var.bucket_name, data.aws_caller_identity.current.account_id)
  acl                                   = "log-delivery-write"
  force_destroy                         = true
  attach_elb_log_delivery_policy        = true
  attach_lb_log_delivery_policy         = true
  attach_deny_insecure_transport_policy = true
  # S3 bucket-level Public Access Block configuration
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
  attach_policy           = true
  policy                  = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailAclCheck20150319",
            "Effect": "Allow",
            "Principal": {"Service":"cloudtrail.amazonaws.com"},
            "Action": "s3:GetBucketAcl",
            "Resource": "arn:aws:s3:::${var.bucket_name}-${data.aws_caller_identity.current.account_id}-log-bucket"
        },
        {
            "Sid": "AWSCloudTrailWrite20150319",
            "Effect": "Allow",
            "Principal": {"Service":"cloudtrail.amazonaws.com"},
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::${var.bucket_name}-${data.aws_caller_identity.current.account_id}-log-bucket/log/AWSLogs/${data.aws_caller_identity.current.account_id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        }
    ]
}
POLICY                                                                        
}

module "kms_key" {
  depends_on  = [data.aws_iam_policy_document.default]
  source      = "clouddrove/kms/aws"
  version     = "0.15.0"

  name        = format("%s-%s-kms", var.bucket_name, data.aws_caller_identity.current.account_id)
  enabled                 = true
  description             = "KMS key for cloudtrail"
  deletion_window_in_days = 15
  policy                  = data.aws_iam_policy_document.default.json
  enable_key_rotation     = true
}

data "aws_iam_policy_document" "default" {
  version = "2012-10-17"
  statement {
    sid    = "Enable IAM User Permissions"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    actions   = ["kms:*"]
    resources = ["*"]
  }
  statement {
    sid    = "Allow CloudTrail to encrypt logs"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
    actions   = ["kms:GenerateDataKey*"]
    resources = ["*"]
    condition {
      test     = "StringLike"
      variable = "kms:EncryptionContext:aws:cloudtrail:arn"
      values   = ["arn:aws:cloudtrail:*:XXXXXXXXXXXX:trail/*"]
    }
  }

  statement {
    sid    = "Allow CloudTrail to describe key"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
    actions   = ["kms:DescribeKey"]
    resources = ["*"]
  }

  statement {
    sid    = "Allow principals in the account to decrypt log files"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    actions = [
      "kms:Decrypt",
      "kms:ReEncryptFrom"
    ]
    resources = ["*"]
    condition {
      test     = "StringEquals"
      variable = "kms:CallerAccount"
      values = [
      "XXXXXXXXXXXX"]
    }
    condition {
      test     = "StringLike"
      variable = "kms:EncryptionContext:aws:cloudtrail:arn"
      values   = ["arn:aws:cloudtrail:*:XXXXXXXXXXXX:trail/*"]
    }
  }

  statement {
    sid    = "Allow alias creation during setup"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    actions   = ["kms:CreateAlias"]
    resources = ["*"]
  }
}