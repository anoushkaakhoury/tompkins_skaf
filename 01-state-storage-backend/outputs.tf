output "_1_bucket_region" {
  value = var.region
}

output "_2_state_bucket_name" {
  value = module.s3_bucket.s3_bucket_id
}

output "_3_dynamodb_table_name" {
  value = aws_dynamodb_table.dynamodb_table.id
}

output "_4_log_bucket_name" {
  value = module.log_bucket.s3_bucket_id
}