variable "bucket_name" {
  default = ""
  type    = string
}

variable "force_destroy" {
  default = false
  type    = bool
}

variable "region" {
  default = ""
  type    = string
}

variable "versioning_enabled" {
  default = false
  type    = bool
}
