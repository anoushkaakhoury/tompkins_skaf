terraform {
  backend "s3" {
    region         = ""
    bucket         = ""
    key            = "dev-tompkins-skaf-stack/network/terraform.tfstate"
    dynamodb_table = ""
  }
}