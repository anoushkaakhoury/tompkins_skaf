data "template_file" "cis_checks" {
  template = file("./templates/cis-checks.json")
}

resource "aws_cloudformation_stack" "cis_checks" {
    name    = "cis-checks-and-alarms"
    parameters = {
    OperatorEMail = var.OperatorEMail
    }
    capabilities = ["CAPABILITY_NAMED_IAM"]
    template_body = join("", data.template_file.cis_checks.*.rendered)
}