variable "additional_tags" {
  description = "Tags for resources "
  type        = map(string)
  default = {
    automation = "true"
  }
}

variable OperatorEMail {
  type        = string
  default     = "xyz@gmail.com"
  description = "Email ID to send alerts and notifications on"
}

variable "region" {
    default   = "us-east-1"
    type      = string
}
