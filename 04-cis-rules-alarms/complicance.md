## CIS Amazon Web Services Foundations v1.4.0

[Link](https://www.cisecurity.org/benchmark/amazon_web_services/)

|   No. | Item                                                                                                              
| ----: | ------------------------------------------------------------------------------------------------------------------ |
|   1.1 | Maintain current contact details                                                                                   |   
|   1.2 | Ensure security contact information is registered                                                                  |   
|   1.3 | Ensure security questions are registered in the AWS account                                                        |   
|   1.4 | Ensure no root account access key exists                                                                           |   
|   1.5 | Ensure MFA is enabled for the "root" account                                                                       |   
|   1.6 | Ensure hardware MFA is enabled for the "root" account                                                              |   
|   1.7 | Eliminate use of the root user for administrative and daily taks                                                   |   
|   1.8 | Ensure IAM password policy requires minimum length of 14 or greater                                                |   
|   1.9 | Ensure IAM password policy prevents password reuse                                                                 |   
|  1.10 | Ensure multi-factor authentication (MFA) is enabled for all IAM users that have a console password                 |   
|  1.11 | Do not setup access keys during initial user setup for all IAM users that have a console password                  |   
|  1.12 | Ensure credentials unused for 45 days or greater are disabled                                                      |   
|  1.13 | Ensure there is only one active access key available for any single IAM user                                       |   
|  1.14 | Ensure access keys are rotated every 90 days or less                                                               |   
|  1.15 | Ensure IAM Users Receive Permissions Only Through Groups                                                           |   
|  1.16 | Ensure IAM policies that allow full "\*:\*" administrative privileges are not created                              |   
|  1.17 | Ensure a support role has been created to manage incidents with AWS Support                                        |   
|  1.18 | Ensure IAM instance roles are used for AWS resource access from instances                                          |   
|  1.19 | Ensure that all the expired SSL/TLS certificates stored in AWS IAM are removed                                     |   
|  1.20 | Ensure that IAM Access analyzer is enabled                                                                         |   
|  1.21 | Ensure IAM users are managed centrally via identity federation or AWS Organizations for multi-account environments |   
| 2.1.1 | Ensure all S3 buckets employ encryption-at-rest                                                                    | 
| 2.1.2 | Ensure S3 Bucket Policy allows HTTPS requests                                                                      | 
| 2.1.3 | Ensure MFA Delete is enable on S3 buckets                                                                          |   
| 2.1.4 | Ensure all data in Amazon S3 has been discovered, classified and secured when required.                            |   
| 2.1.5 | Ensure that S3 Buckets are configured with 'Block public access(bucket settings)'                                  |  
| 2.2.1 | Ensure EBS volume encryption is enabled                                                                            |   
| 2.3.1 | Ensure that encryption is enabled for RDS instances                                                                |  
|   3.1 | Ensure CloudTrail is enabled in all regions                                                                        |   
|   3.2 | Ensure CloudTrail log file validation is enabled                                                                   |  
|   3.3 | Ensure the S3 bucket used to store CloudTrail logs is not publicly accessible                                      |   
|   3.4 | Ensure CloudTrail trails are integrated with CloudWatch Logs                                                       |  
|   3.5 | Ensure AWS Config is enabled in all regions                                                                        |   
|   3.6 | Ensure S3 bucket access logging is enabled on the CloudTrail S3 bucket                                             |   
|   3.7 | Ensure CloudTrail logs are encrypted at rest using KMS CMKs                                                        |   
|   3.8 | Ensure rotation for customer created CMKs is enabled                                                               | 
|   3.9 | Ensure VPC flow logging is enabled in all VPCs                                                                     | 
|  3.10 | Ensure that Object-level logging for write events is enables for S3 bucket                                         |   
|  3.11 | Ensure that Object-level logging for read events is enables for S3 bucket                                          |   
|   4.1 | Ensure a log metric filter and alarm exist for unauthorized API calls                                              |   
|   4.2 | Ensure a log metric filter and alarm exist for Management Console sign-in without MFA                              |  
|   4.3 | Ensure a log metric filter and alarm exist for usage of "root" account                                             |  
|   4.4 | Ensure a log metric filter and alarm exist for IAM policy changes                                                  |   
|   4.5 | Ensure a log metric filter and alarm exist for CloudTrail configuration changes                                    |  
|   4.6 | Ensure a log metric filter and alarm exist for AWS Management Console authentication failures                      |  
|   4.7 | Ensure a log metric filter and alarm exist for disabling or scheduled deletion of customer created CMKs            |   
|   4.8 | Ensure a log metric filter and alarm exist for S3 bucket policy changes                                            |  
|   4.9 | Ensure a log metric filter and alarm exist for AWS Config configuration changes                                    |   
|  4.10 | Ensure a log metric filter and alarm exist for security group changes                                              |  
|  4.11 | Ensure a log metric filter and alarm exist for changes to Network Access Control Lists (NACL)                      | 
|  4.13 | Ensure a log metric filter and alarm exist for route table changes                                                 |   
|  4.14 | Ensure a log metric filter and alarm exist for VPC changes                                                         |   
|  4.15 | Ensure a log metric filter and alarm exist for AWS Organizations changes                                           |  
|   5.1 | Ensure no Network ACLs allow ingress from 0.0.0.0/0 to remote server administration ports                          | 
|   5.2 | Ensure no security groups allow ingress from 0.0.0.0/0 to remote server administration ports                       | 
|   5.3 | Ensure the default security group of every VPC restricts all traffic                                               |   
|   5.4 | Ensure routing tables for VPC peering are "least access"                                                           |  