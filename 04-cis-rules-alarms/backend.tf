terraform {
  backend "s3" {
    region         = ""
    bucket         = ""
    key            = "terraform-stack-poc/cis-rules-alarms/terraform.tfstate"
    dynamodb_table = ""
  }
}