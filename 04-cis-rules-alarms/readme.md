# USAGE

- Used to create AWS config rules and CLoudwatch alarms with SNS topic for checking CIS compliance. [CIS Amazon Web Services Foundations Benchmark  v1.4.0 ] 
- A Cloudformation template is used to create all the rules and alarms and the SNS topic. 
- User can enable Email subscription for SNS topic. 
- Other subscriptions can be added manually in the SNS topic. 

- To store the ***.tfstate*** file in provisioned cloud backend edit ***backend.tf***.

```
NOTE: The paremeter KEY in backend.tf is the path in the backend S3 bucket where the .tfstate for each module will be stored. Make sure that the path is not the same for different modules.
```

- To pass custom values edit one of the file ***.tfvars.reference*** in vars folder and change it to ***.tfvars*** and pass your values there.

- To apply  :

        cd aws/07-cis-rules-alarms
        terraform init
        terraform plan -var-file=vars/terraform.tfvars -out plan.out   
        terraform apply plan.out 

- To destroy the resources:

      terraform destroy -var-file=vars/terraform.tfvars

# CIS COMPLIANCE 

- This module creates AWS Config Rules and CLoudwatch Alarams as per CIS Amazon Web Services Foundations Benchmark  v1.4.0 recommendations. 
- Refre to the file ***compliance.md*** for checking a list of CLoudwatch alarsma and Config rules that will be deployed. 
